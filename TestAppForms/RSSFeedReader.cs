﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Net.Http;
using Newtonsoft.Json.Linq;

public class RSSFeedReader {
    /*
	public static async Task<List<FeedItem>> getNewsItems(string url){
		List<FeedItem> feedItems = new List<FeedItem> ();

		string content = await loadFeed (url);
		XDocument doc = XDocument.Parse (content);

		foreach (XElement item in doc.Descendants("item")) {
			feedItems.Add (new FeedItem {
				title = item.Element ("title").Value,
				id = item.Element ("link").Value,
				date = item.Element ("pubDate").Value 
			});

		}

		return feedItems;
	}*/

	async static Task<string> loadFeed(string url) { 
		HttpClient client = new HttpClient();
		Task<string> getStringTask = client.GetStringAsync(url);
		string content = await client.GetStringAsync (url);
		return content;
	}

    public static async Task<List<FeedItem>> getNewsItemsFromApi(){
        List<FeedItem> items = new List<FeedItem>();

        string content = await loadFeed("http://eurnu.nl/cgi-bin/as.cgi/0353000/c/api?id=vjdepap9lbl1&f=news");
        var json = JObject.Parse(content);

        var status = json["status"].ToString();

        if (status.Equals("ok"))
        {
            var news = json["news"];
            foreach (var item in news)
            {
                FeedItem fi = new FeedItem {
                    title = item["title"].ToString(),
                    id = item["id"].ToString(), 
                    date = item["date"].ToString()
                };

                items.Add(fi);
            }
        }       
        return items; 
    }

    public static async Task<NewsItem> getItemInfo(string bagger) {
        string content = await loadFeed("http://eurnu.nl/cgi-bin/as.cgi/0353000/c/api?id=vjdepap9lbl1&f=newsitem&itemid=" + bagger);
        var json = JObject.Parse(content);

        var status = json["status"].ToString();

        if(status.Equals("ok")) {

            return new NewsItem
            {
                title = JObject.Parse(json["item"].ToString())["title"].ToString(),
                text = JObject.Parse(json["newsItem"].ToString())["text"].ToString()
            };

        }

        return null;
    }
}

public class FeedItem {
	public string title { get; set; }
	public string id { get; set; }
	public string date { get; set; }
}

public class NewsItem {
    public string title { get; set; }
    public string text { get; set; }
   

}

﻿
using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace TestAppForms
{
	public partial class feedPage : ContentPage
	{
		public feedPage () {
			InitializeComponent ();
			setUpList ();
		}

		async void setUpList() {
			txtInit.Text = "Laden...";
            List<FeedItem> itemList = await RSSFeedReader.getNewsItemsFromApi();

			txtInit.Text = "Berichten";
			feedList.ItemsSource = itemList; 

			feedList.ItemTemplate = new DataTemplate (typeof(TextCell));
			feedList.ItemTemplate.SetBinding (TextCell.TextProperty, "title" );
          
			feedList.ItemTapped += (sender, e) => {
				//FeedItem selectedItem = (FeedItem) ((ListView)sender).SelectedItem;
				//Device.OpenUri(new Uri(selectedItem.link));
                Navigation.PushModalAsync(new webPage((FeedItem)((ListView)sender).SelectedItem));
			};

		}
	}
}
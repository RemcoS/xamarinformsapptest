﻿using Xamarin.Forms;

namespace TestAppForms
{
    public partial class webPage : ContentPage
    {
        public webPage(FeedItem item)
        {
            InitializeComponent();

            txtTitle.Text = item.id;
            setUpNewsItem(item);
        }


        async void setUpNewsItem(FeedItem item) {
            NewsItem newsItem = await RSSFeedReader.getItemInfo(item.id);
            txtTitle.Text = newsItem.title;
            txtItem.Text = newsItem.text;
            
        }
    }
}

